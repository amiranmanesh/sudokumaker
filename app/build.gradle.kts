import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    kotlin("android.extensions")
    id("com.google.gms.google-services")
    id("com.google.firebase.firebase-perf")
    id("com.google.firebase.crashlytics")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.Build.compileSdk)
    buildToolsVersion(Versions.Build.buildTools)

    defaultConfig {
        applicationId = "ir.sudoku.maker"
        minSdkVersion(Versions.Build.minSdk)
        targetSdkVersion(Versions.Build.targetSdk)
        versionCode = Versions.versionCode
        versionName = Versions.versionName

        multiDexEnabled = true
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    signingConfigs {
        create("release") {
            rootProject.file("key.properties").takeIf { it.exists() }?.let { file ->
                val properties = Properties().also { it.load(file.reader()) }
                storeFile = file(properties["storeFile"].toString())
                keyAlias = properties["keyAlias"].toString()
                keyPassword = properties["keyPassword"].toString()
                storePassword = properties["storePassword"].toString()
            }
        }
    }

    buildTypes {
        named("release") {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            setProguardFiles(
                listOf(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            )
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.Kotlin.coroutinesCore)
    implementation(Libs.Kotlin.stdlib)
    implementation(Libs.AndroidX.core)
    implementation(Libs.AndroidX.appCompat)
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.AndroidX.multiDex)
    implementation(Libs.AndroidX.cardView)
    implementation(Libs.AndroidX.recyclerView)
    implementation(Libs.AndroidX.media)
    implementation(Libs.AndroidX.exIfInterface)
    implementation(Libs.AndroidX.vectorDrawable)
    implementation(Libs.AndroidX.legacy)
    implementation(Libs.Logging.pulp)
    implementation(Libs.Rx.rxJava2)
    implementation(Libs.Rx.rxAndroid2)
    implementation(Libs.Glide.core)
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    kapt(Libs.Glide.coreCompiler)
    implementation(Libs.Network.Retrofit.core)
    implementation(Libs.Network.Retrofit.rxJava2)
    implementation(Libs.Network.Retrofit.converterGson)
    implementation(Libs.Network.Retrofit.converterScalars)
    implementation(Libs.Network.OkHttp.loggingInterceptor)
    implementation(Libs.Google.material)
    implementation(Libs.Google.Firebase.core)
    implementation(Libs.Google.Firebase.iid)
    implementation(Libs.Google.Firebase.messaging)
    implementation(Libs.Google.Firebase.analytics)
    implementation(Libs.Google.Firebase.perf)
    implementation(Libs.Google.Firebase.crash)
    implementation(Libs.Google.Firebase.config)
    implementation(Libs.Room.runtime)
    implementation(Libs.Room.rxJava2)
    implementation(Libs.Room.ktx)
    implementation(Libs.Arch.extensions)
    implementation(Libs.Arch.viewmodel)
    kapt(Libs.Room.compiler)
    implementation(Libs.WorkManager.runtime)
    implementation(Libs.WorkManager.rxJava2)
    implementation(Libs.Font.calligraphy3)
    implementation(Libs.Font.viewPump)
    implementation(Libs.SeekBar.circularSeekBar)

    testImplementation(Libs.Test.junit)
    androidTestImplementation(Libs.AndroidX.Test.junitExt)
    androidTestImplementation(Libs.AndroidX.Test.espressoCore)
}
