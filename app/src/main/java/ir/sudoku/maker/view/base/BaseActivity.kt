package ir.sudoku.maker.view.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.malv.utils.Pulp

abstract class BaseActivity : AppCompatActivity() {
    private val TAG = "BaseActivity"

    /**
     * To use and configure font
     * Calligraphy used to configure font.
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onStart() {
        super.onStart()
        log(this.localClassName, "onStart")
    }

    override fun onStop() {
        super.onStop()
        log(this.localClassName, "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        log(this.localClassName, "onRestart")
    }

    override fun onResume() {
        super.onResume()
        log(this.localClassName, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(this.localClassName, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(this.localClassName, "onDestroy")
    }
    //End

    @SuppressLint("ObsoleteSdkInt", "SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set orientation to Portrait
        log(this.localClassName, "onCreate")

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        }
        supportActionBar?.hide()

    }


    fun isShowFragment(tag: String): Boolean {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        return fragment != null
    }

    fun hideFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.hide(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    fun removeFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.remove(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    fun showFragment(tag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()
        fragment?.let { ft.show(it) }

        try {
            ft.commit()
        } catch (e: IllegalStateException) {
            ft.commitAllowingStateLoss()
        }
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "Submitting Event") {
            "Screen" to screenName
            "Event" to event
        }
        try {
            val analytics = FirebaseAnalytics.getInstance(this)
            val b = Bundle()
            b.putString("screen_name", screenName)
            b.putString("event", event)
            analytics.logEvent("log", b)
        } catch (e: Exception) {
            Pulp.error(TAG, "Error logging with fireBase", e)
        }

    }

}