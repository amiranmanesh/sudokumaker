package ir.sudoku.maker.view.base

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.analytics.FirebaseAnalytics
import ir.malv.utils.Pulp
import ir.sudoku.maker.utilities.DeviceUtils

abstract class BaseBottomDialog : BottomSheetDialogFragment() {

    protected lateinit var contexts: Context
    protected lateinit var views: View

    companion object {
        var TAG = "BaseBottomDialog"
    }

    /**
     * Logging into fireBase analytics for each activity
     */
    override fun onResume() {
        super.onResume()
        log(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(TAG, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(TAG, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        log(TAG, "onDetach")
    }
    //End

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        DeviceUtils.toastShort(contexts, message)
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        DeviceUtils.toastLong(contexts, message)
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "$screenName -> $event")
        try {
            val analytics = FirebaseAnalytics.getInstance(contexts)
            val b = Bundle()
            b.putString("screen_name", screenName)
            b.putString("event", event)
            analytics.logEvent("log", b)
        } catch (e: Exception) {
            Pulp.error(TAG, "Error logging with fireBase", e)
        }

    }

    fun removeFragment(tag: String) {
        val fragment = fragmentManager?.findFragmentByTag(tag)
        val ft = fragmentManager?.beginTransaction()
        fragment?.let { ft?.remove(it) }
        ft?.commit()
    }

    fun showMessageOKCancel(
        context: Context,
        message: String,
        okListener: DialogInterface.OnClickListener,
        noListener: DialogInterface.OnClickListener
    ) {
        androidx.appcompat.app.AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton("باشه", okListener)
            .setNegativeButton("بعدا", noListener)
            .create()
            .show()
    }

}