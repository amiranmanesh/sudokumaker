package ir.sudoku.maker.utilities


object Constant {

    // Debugging log tag
    var DEBUG_MODE = true
    const val LOG_TAG = "SudokuMakerApp"
}