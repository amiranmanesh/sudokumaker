package ir.sudoku.maker.utilities

import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import ir.malv.utils.Pulp
import ir.sudoku.maker.R

object DeviceUtils {

    /**
     * @return DisplayWidth
     */
    fun getDisplayWidth(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)

        return metrics.widthPixels
    }

    /**
     * @return DisplayHeight
     */
    fun getDisplayHeight(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val metrics = DisplayMetrics()
        display.getMetrics(metrics)

        return metrics.heightPixels
    }


    fun showSnackBar(
        view: View,
        context: Context,
        msg: String = "ارتباط خود را با اینترنت بررسی کنید.",
        btnTitle: String = "بررسی مجدد",
        isForEver: Boolean = true,
        bar: () -> Unit
    ): Snackbar {
        val snackBar = if (isForEver) Snackbar
            .make(
                view,
                msg,
                Snackbar.LENGTH_INDEFINITE
            )
            .setAction(btnTitle) {
                bar()
            }
        else
            Snackbar
                .make(
                    view,
                    msg,
                    Snackbar.LENGTH_LONG
                )

        snackBar.setActionTextColor(Color.WHITE)
        val sbView = snackBar.view
        sbView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        sbView.setBackgroundColor(context.resources.getColor(R.color.white))
        val textView = sbView.findViewById(R.id.snackbar_text) as TextView
        val textView2 = sbView.findViewById(R.id.snackbar_action) as TextView
        textView.setTextColor(context.resources.getColor(R.color.black))
        textView.compoundDrawablePadding = 15
        textView.setPadding(0, 5, 0, 0)
        textView.setCompoundDrawablesWithIntrinsicBounds(
            null,
            null,
            null,
            null
        )
        textView2.setTextColor(context.resources.getColor(R.color.colorAccent))
        snackBar.show()

        return snackBar
    }

    fun toastShort(context: Context?, msg: String?) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Pulp.wtf("toastShort", "Exception", e)
        }
    }

    fun toastLong(context: Context?, msg: String?) {
        try {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            Pulp.wtf("toastShort", "Exception", e)
        }
    }

}