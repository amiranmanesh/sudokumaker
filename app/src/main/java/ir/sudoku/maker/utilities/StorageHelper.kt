package ir.sudoku.maker.utilities

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StorageHelper(context: Context) {

    private val prefName = "SudokuMaker"

    private val settings: SharedPreferences =
        context.getSharedPreferences(prefName, Context.MODE_PRIVATE)

    fun setString(key: String, value: String?) = settings.edit().putString(key, value).apply()

    fun setInt(key: String, value: Int) = settings.edit().putInt(key, value).apply()

    fun setBool(key: String, value: Boolean) = settings.edit().putBoolean(key, value).apply()

    fun setMap(key: String, map: Map<String, Any?>) =
        settings.edit().putString(key, Gson().toJson(map)).apply()


    fun getString(key: String, defValue: String?) = settings.getString(key, defValue)

    fun getInt(key: String, defValue: Int) = settings.getInt(key, defValue)

    fun getBool(key: String, defValue: Boolean) = settings.getBoolean(key, defValue)

    fun getMap(key: String, defValue: Map<String, String> = emptyMap()): Map<String, Any?> {
        return try {
            Gson().fromJson(
                settings.getString(key, "{}"),
                object : TypeToken<Map<String, Any?>>() {}.type
            )
        } catch (e: Exception) {
            defValue
        }
    }

}