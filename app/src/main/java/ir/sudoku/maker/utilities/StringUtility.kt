package ir.sudoku.maker.utilities

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.malv.utils.Pulp
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

object StringUtility {
    const val TAG = "StringUtility"

    fun stringFromNumbers(vararg numbers: Int): String {
        val sNumbers = StringBuilder()
        for (number in numbers) sNumbers.append(number)
        return sNumbers.toString()
    }

    /**
     * This method use to convert income string to Map<String,String>
     */
    fun stringToMap(jsonString: String?): Map<String, String> {
        if (jsonString == null) return emptyMap()
        return try {
            Gson().fromJson<HashMap<String, String>>(
                jsonString,
                object : TypeToken<HashMap<String, String>>() {}.type
            )
        } catch (e: JSONException) {
            Pulp.error(TAG, "Failed to convert message to map", e)
            emptyMap()
        }
    }


    fun String.toJSONObject(): JSONObject {
        return try {
            JSONObject(this)
        } catch (e: Exception) {
            JSONObject()
        }
    }


    fun thousandSeparator(input: String?): String {
        if (input == null || input == "null") return "0"
        val number = java.lang.Long.parseLong(input)
        return try {
            val decimalFormat = DecimalFormat()
            val decimalFormatSymbol = DecimalFormatSymbols()
            decimalFormatSymbol.groupingSeparator = ','
            decimalFormat.decimalFormatSymbols = decimalFormatSymbol
            decimalFormat.format(number)
        } catch (ex: Exception) {
            number.toString()
        }
    }

}