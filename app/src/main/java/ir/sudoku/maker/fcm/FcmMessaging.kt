package ir.sudoku.maker.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import ir.malv.utils.Pulp
import ir.sudoku.maker.R
import ir.sudoku.maker.view.splash.SplashActivity
import java.util.*

class FcmMessaging : FirebaseMessagingService() {

    companion object {
        const val TAG = "FCM"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Pulp.info(TAG, "Message RECEIVED\n remoteMessage: $remoteMessage")

        if (!remoteMessage.data.isNullOrEmpty()) {
//            val worker = FcmMessageParser.getWorker(remoteMessage)
//            remoteMessage?.data?.let { w
//            orker.execute(it) }
        }
        if (remoteMessage.notification != null) {
            sendNotification(remoteMessage.notification?.title, remoteMessage.notification?.body)
        }


    }

    override fun onNewToken(token: String) {
        //TODO handle new token
//        val worker = RefreshTokenTask::class
//        worker.execute(mapOf("token" to token))
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    private fun sendNotification(messageTitle: String?, messageBody: String?) {
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)


        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel)
        }

        Objects.requireNonNull(notificationManager)
            .notify(0 /* ID of notification */, notificationBuilder.build())
    }

}