package ir.sudoku.maker

import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import ir.malv.utils.Pulp
import ir.sudoku.maker.utilities.Constant

class SudokuMakerApp : MultiDexApplication() {

    companion object {
        const val TAG = "Application"
    }

    /**
     * will set font with calligraphy in all of the application
     */
    override fun onCreate() {
        super.onCreate()
        Pulp.init(this).setMainTag(Constant.LOG_TAG)
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/iransans.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

//        Thread.setDefaultUncaughtExceptionHandler { t, e ->
//            Pulp.wtf(
//                TAG,
//                "** Uncaught exception occurred **\n Thread: ${t.name}\n Cause: ${e.message}",
//                e
//            )
//        }
//        RxJavaPlugins.setErrorHandler { throwable ->
//            Pulp.wtf(
//                TAG,
//                "** Uncaught exception occurred **\n Cause: ${throwable.message}",
//                throwable
//            )
//        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}