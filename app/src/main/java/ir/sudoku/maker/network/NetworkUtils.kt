package ir.sudoku.maker.network

import android.annotation.SuppressLint
import ir.malv.utils.Pulp
import ir.sudoku.maker.utilities.Constant.DEBUG_MODE
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object NetworkUtils {

    /**
     * The retrofit should be singleton everywhere
     * If we need this we must use this class
     */
    private var retrofit: Retrofit? = null

    /**
     * Returns the retrofit
     * If it was null creates it
     * Otherwise simply returns it
     *
     * @param baseUrl is the url set as base url of requests
     */
    fun retrofit(baseUrl: String, token: String? = null): Retrofit {

        val logging = HttpLoggingInterceptor { message ->
            if (DEBUG_MODE)
                Pulp.sout(message)
        }
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor { chain ->
                @SuppressLint("HardwareIds")
                val request: Request = if (token != null) {
                    chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", "Bearer $token")
                        .build()
                } else {
                    chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .build()
                }

//                val response = chain.proceed(request)
//                val worker = ResponseErrorParser.getWorker(response.code())
//                worker.execute(response.code())

                chain.proceed(request)
            }
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit!!
    }
}