buildscript {
    repositories {
        google()
        jcenter()
        maven { setUrl(Libs.Maven.jitPack) }
    }
    dependencies {
        classpath(kotlin("gradle-plugin", version = Libs.Kotlin.version))
        classpath(Libs.androidGradlePlugin)
        classpath(Libs.androidGoogleServicesPlugin)
        classpath(Libs.androidFireBasePrefPlugin)
        classpath(Libs.androidFireBaseCrashPlugin)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { setUrl(Libs.Maven.jitPack) }
    }
}

tasks.register<Delete>("clean").configure {
    delete(rootProject.buildDir)
}
