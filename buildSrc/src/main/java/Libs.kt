object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:4.0.1"
    const val androidGoogleServicesPlugin = "com.google.gms:google-services:4.3.3"
    const val androidFireBasePrefPlugin = "com.google.firebase:perf-plugin:1.3.1"
    const val androidFireBaseCrashPlugin = "com.google.firebase:firebase-crashlytics-gradle:2.2.0"

    object Maven {
        const val jitPack = "https://jitpack.io"
    }

    object AndroidX {
        const val core = "androidx.core:core-ktx:1.3.1"
        const val multiDex = "androidx.multidex:multidex:2.0.1"
        const val appCompat = "androidx.appcompat:appcompat:1.2.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:1.1.3"
        const val cardView = "androidx.cardview:cardview:1.0.0"
        const val recyclerView = "androidx.recyclerview:recyclerview:1.1.0"
        const val media = "androidx.media:media:1.1.0"
        const val exIfInterface = "androidx.exifinterface:exifinterface:1.2.0"
        const val vectorDrawable = "androidx.vectordrawable:vectordrawable-animated:1.1.0"
        const val legacy = "androidx.legacy:legacy-support-v4:1.0.0"

        object Test {
            const val espressoCore = "androidx.test.espresso:espresso-core:3.2.0"
            const val junitExt = "androidx.test.ext:junit:1.1.1"
        }
    }

    object Google {
        const val material = "com.google.android.material:material:1.2.0"

        object Firebase {
            const val core = "com.google.firebase:firebase-core:17.5.0"
            const val iid = "com.google.firebase:firebase-iid:20.2.4"
            const val messaging = "com.google.firebase:firebase-messaging:20.2.4"
            const val analytics = "com.google.firebase:firebase-analytics-ktx:17.5.0"
            const val perf = "com.google.firebase:firebase-perf:19.0.8"
            const val crash = "com.google.firebase:firebase-crashlytics-ktx:17.2.1"
            const val config = "com.google.firebase:firebase-config-ktx:19.2.0"
        }
    }

    object Network {
        object Retrofit {
            const val core = "com.squareup.retrofit2:retrofit:2.6.4"
            const val rxJava2 = "com.squareup.retrofit2:adapter-rxjava2:2.5.0"
            const val converterGson = "com.squareup.retrofit2:converter-gson:2.6.4"
            const val converterScalars = "com.squareup.retrofit2:converter-scalars:2.5.0"
        }

        object OkHttp {
            const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:3.12.12"
        }
    }

    object Room {
        const val runtime = "androidx.room:room-runtime:2.2.5"
        const val rxJava2 = "androidx.room:room-rxjava2:2.2.5"
        const val ktx = "androidx.room:room-ktx:2.2.5"
        const val compiler = "androidx.room:room-compiler:2.2.5"
    }

    object WorkManager {
        const val runtime = "androidx.work:work-runtime-ktx:2.4.0"
        const val rxJava2 = "androidx.work:work-rxjava2:2.4.0"
    }

    object Font {
        const val calligraphy3 = "io.github.inflationx:calligraphy3:3.1.1"
        const val viewPump = "io.github.inflationx:viewpump:2.0.3"
    }

    object Glide {
        const val core = "com.github.bumptech.glide:glide:4.11.0"
        const val coreCompiler = "com.github.bumptech.glide:compiler:4.9.0"
    }

    object SeekBar {
        const val circularSeekBar = "me.tankery.lib:circularSeekBar:1.2.0"
    }

    object Kotlin {
        const val version = "1.4.0"
        const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:1.4.0"
    }

    object Rx {
        const val rxJava2 = "io.reactivex.rxjava2:rxjava:2.2.19"
        const val rxAndroid2 = "io.reactivex.rxjava2:rxandroid:2.1.1"
    }

    object Arch {
        const val extensions = "android.arch.lifecycle:extensions:1.1.1"
        const val viewmodel = "android.arch.lifecycle:viewmodel:1.1.1"
    }

    object Logging {
        const val pulp = "ir.malv.utils:pulp:0.2.0"
    }

    object Test {
        const val junit = "junit:junit:4.12"
    }

}